+ [Brain](#brain)
+ [Congenital Disorders](#congenital-disorders)
+ [General](#general)
+ [NEWS](#news)
+ [Papers](#papers)
   - [Podcast](#podcast)
+ [Projects](#projects)

----

# Brain
* Medulla oblongata :: http://en.wikipedia.org/wiki/Medulla_oblongata
   * Anterior median fissure of the medulla oblongata :: http://en.wikipedia.org/wiki/Anterior_median_fissure_of_the_medulla_oblongata
* Pons :: http://en.wikipedia.org/wiki/Pons
* Cerebellum :: http://en.wikipedia.org/wiki/Cerebellum
* White matter :: http://en.wikipedia.org/wiki/White_matter
* Gracile nucleus :: http://en.wikipedia.org/wiki/Gracile_nucleus
* Cuneate nucleus :: http://en.wikipedia.org/wiki/Cuneate_nucleus
* Medial lemniscus :: http://en.wikipedia.org/wiki/Medial_lemniscus 
* Perikaryon {Soma /somata /somas, perikarya, or cyton} :: http://en.wikipedia.org/wiki/Perikaryon
* Internal arcuate fibers :: http://en.wikipedia.org/wiki/Internal_arcuate_fibers
* Alar plate :: http://en.wikipedia.org/wiki/Alar_plate
* Inferior cerebellar peduncle :: http://en.wikipedia.org/wiki/Inferior_cerebellar_peduncle
* Glossopharyngeal nerve :: http://en.wikipedia.org/wiki/Glossopharyngeal_nerve
* Cranial neural crest :: http://en.wikipedia.org/wiki/Cranial_neural_crest
* Parafollicular cell :: http://en.wikipedia.org/wiki/Parafollicular_cell
* Endocranium :: http://en.wikipedia.org/wiki/Endocranium
* Foramen magnum :: http://en.wikipedia.org/wiki/Foramen_magnum
* Spinal nerve :: http://en.wikipedia.org/wiki/Spinal_nerve  
* Posterior spinal artery :: http://en.wikipedia.org/wiki/Posterior_spinal_artery
* Efferent nerve fiber :: http://en.wikipedia.org/wiki/Efferent_nerve_fiber
* Sternohyoid muscle :: http://en.wikipedia.org/wiki/Sternohyoid_muscle
* Sternothyroid muscle :: http://en.wikipedia.org/wiki/Sternothyroid_muscle
* Omohyoid muscle :: http://en.wikipedia.org/wiki/Omohyoid_muscle
+ https://en.wikipedia.org/wiki/Grandmother_cell

----

# [Congenital Disorders](http://en.wikipedia.org/wiki/Category:Congenital_disorders)
**REF : Developmental Biology**
+ [Heterotopia](http://en.wikipedia.org/wiki/Heterotopia_%28medicine%29)
+ [Gray matter heterotopia](http://en.wikipedia.org/wiki/Gray_matter_heterotopia)

----

# General
* Neurophysiology :: http://en.wikipedia.org/wiki/Neurophysiology
* Neurology :: http://en.wikipedia.org/wiki/Neurology

----

# NEWS
* [Activity in Dendrites is Critical in Memory Formation](http://neurosciencenews.com/neuroscience-dendrite-memory-formation-1477/). 

----

# Neuroethics
* [How do we make moral judgements? IRCM ethics experts open the black box of moral intuitions](http://www.rc-rc.ca/blog/how-do-we-make-moral-judgements-ircm-ethics-experts-open-the-black-box-of-moral-intuitions)

---- 

# Papers
* The Telltale Hand: [How Writing Reveals the Damaged Brain](http://www.dana.org/Cerebrum/Default.aspx?id=39304) By: Marc J. Seifer Ph.D.
* [Left Hand, Left Brain: The Plot Thickens](http://www.dana.org/Cerebrum/2005/Left_Hand,_Left_Brain__The_Plot_Thickens/) By: Carolyn Asbury Ph.D. 
* PAPER (pdf in the 'oa-papers' folder): [Zebra Finch Mates Use Their Forebrain Song System in Unlearned Call Communication](http://www.plosone.org/article/info%3Adoi%2F10.1371%2Fjournal.pone.0109334), Andries Ter Maat, et al, Published: October 14, 2014, DOI: 10.1371/journal.pone.0109334.
* [Mental Leaps Cued by Memory’s Ripples](http://www.quantamagazine.org/20141022-mental-leaps-cued-by-memorys-ripples/)
* [Mathematical model shows how the brain remains stable during learning](http://www.riken.jp/en/pr/press/2014/20141023_1/)
* [Human Intuition: Its Powers and Perils](http://www.dana.org/Cerebrum/2002/Human_Intuition__The_Brain_Behind_the_Scenes/) By: David G. Myers Ph.D. 
* [The Creating Brain: The Neuroscience of Genius](http://www.dana.org/Cerebrum/2005/The_Creating_Brain__The_Neuroscience_of_Genius/) By: Nancy C. Andreasen M.D., Ph.D.
* [Calcium transient prevalence across the dendritic arbour predicts place field properties](http://www.nature.com/nature/journal/vaop/ncurrent/full/nature13871.html)

### Podcast
* Podcast: [The Mind's Hidden Switches](http://www.scientificamerican.com/podcast/episode/the-minds-hidden-switches-11-11-22/) by E.Nestler, 22/Nov/2011.

----

# Projects
* [BrainMaps](http://en.wikipedia.org/wiki/BrainMaps) project.

