
# .f
+ [Fortran-wiki Tutorials](http://fortranwiki.org/fortran/show/Tutorials)
+ [Fortran95](http://www-eio.upc.edu/lceio/manuals/Fortran95-manual.pdf) manual.
+ Fortran [Basic](http://nf.nci.org.au/training/FortranBasic/)
+ Fortran [Advanced](http://nf.nci.org.au/training/FortranAdvanced/)
+ Documentation for the [Intel8 Fortran compiler](http://nf.nci.org.au/facilities/software/Compilers/Intel8/doc/fcompindex.htm).


### Syntax
+ [module objects](http://www.coria-cfd.fr/index.php/SiTCom-B#Implementation) and [indices block](http://www.coria-cfd.fr/index.php/SiTCom-B#Blocks)

**corner indices**
+ [corner indices](https://books.google.co.in/books?id=oQirAAAAIAAJ&pg=PA117&lpg=PA117&dq=corner+indices&source=bl&ots=Fyj3PnnX-R&sig=TsRA_-eZ427x8cM4AdkVC7l0Yug&hl=en&sa=X&ei=1TDcVOChK4K1uAT3_4DACQ&ved=0CFMQ6AEwCg#v=onepage&q=corner%20indices&f=false)
+ [corner indices](https://books.google.co.in/books?id=q4PBBAAAQBAJ&pg=PA52&lpg=PA52&dq=corner+indices&source=bl&ots=T25_s3t1H0&sig=BkmpxbXwjoNeiX1Mm-qD1eARhNw&hl=en&sa=X&ei=f0vcVIj1K4OcuQTxqYC4Bg&ved=0CBwQ6AEwADgK#v=onepage&q=corner%20indices&f=false) from Traces and Emergence of Nonlinear Programming.




