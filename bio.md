**Bioinformatics, biotechnology, biostatistics, ...**

* [BOOKS](#books)
* [BIOINFORMATICS](#bioinformatics)
* [BIOSTATISTICS](#bioinformatics)
* [BIOTECHNOLOGY](#biotechnology)
   
----

# BOOKS
* Free [Biology textbook](http://openstaxcollege.org/textbooks/biology/resources).

----

# BIOINFORMATICS
* https://en.wikipedia.org/wiki/Bioinformatics
* https://en.wikipedia.org/wiki/Category:Genetics 
* https://en.wikipedia.org/wiki/Genetics
* https://en.wikipedia.org/wiki/Recombinant_DNA

----

# BIOSTATISTICS
* https://en.wikipedia.org/wiki/Biostatistics
* https://en.wikipedia.org/wiki/Mathematical_and_theoretical_biology
* https://en.wikipedia.org/wiki/BBGKY_hierarchy
* [HN thread on Bayesian Statistics](https://news.ycombinator.com/item?id=4030061) and the article: [Understand the Math Behind it All: Bayesian Statistics](http://blogs.adobe.com/digitalmarketing/personalization/conversion-optimization/understand-the-math-behind-it-all-bayesian-statistics/)

----

# BIOTECHNOLOGY
* https://en.wikipedia.org/wiki/Biotechnology
* https://en.wikipedia.org/wiki/Biomedical_sciences
* https://en.wikipedia.org/wiki/Biomedical_research
* https://en.wikipedia.org/wiki/Cell_biology 
* https://en.wikipedia.org/wiki/Organisms
* https://en.wikipedia.org/wiki/Protein
* https://en.wikipedia.org/wiki/Genomics 

### SAS
+ http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2867006/
+ https://en.wikipedia.org/wiki/Small-angle_scattering
+ http://www.sasbdb.org/
+ https://en.wikipedia.org/wiki/Biological_small-angle_scattering

### Proteins
+ https://www.ebi.ac.uk/Tools/webservices/
+ https://www.ebi.ac.uk/pdbe/nmr-resources
+ https://www.ebi.ac.uk/pdbe/entry/search/index?text:NMR
+ https://www.ebi.ac.uk/services/proteins
* https://www.ebi.ac.uk/pdbe/entry/pdb/3wvg
* https://www.ebi.ac.uk/Tools/webservices/services/pfa/fingerprintscan_rest

----


