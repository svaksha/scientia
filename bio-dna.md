* [DNA](#dna)
   * [Chromosomes](#chromosomes)
   * [Sequence Analysis](#sequence-analysis)
* [PAPERS](#papers)

----

# DNA
* Mitochondrial DNA :: http://en.wikipedia.org/wiki/Mitochondrial_DNA
* Palaeo-Eskimo, an ancient-human :: http://en.wikipedia.org/wiki/Palaeo-Eskimo
* Nuclear DNA :: http://en.wikipedia.org/wiki/Nuclear_DNA
* Denisova hominin's mtDNA :: http://en.wikipedia.org/wiki/Denisova_hominin
* http://en.wikipedia.org/wiki/MtDNA 
* Ancient DNA :: http://en.wikipedia.org/wiki/Ancient_DNA
* Alternative splicing :: http://en.wikipedia.org/wiki/Alternative_splicing
* Single gene disorder :: http://en.wikipedia.org/wiki/Genetic_disorder#Single_gene_disorder
* Significance analysis of microarrays: http://en.wikipedia.org/wiki/Significance_analysis_of_microarrays
* Gene Ontology: http://en.wikipedia.org/wiki/Gene_Ontology
* Contig :: http://en.wikipedia.org/wiki/Contig
* ORF :: http://en.wikipedia.org/wiki/Open_reading_frames
* Gene expression profiling, http://en.wikipedia.org/wiki/Gene_expression_profiling
* [DNA (Deoxyribonucleic acid)](http://en.wikipedia.org/wiki/DNA)
* DNA mutations and changes, http://www.genetichealth.com/g101_changes_in_dna.shtml
+ [Variomes](http://en.wikipedia.org/wiki/Variome)
+ http://en.wikipedia.org/wiki/Deoxyribonucleotide

## DNA replication
+ [Genetic code](http://en.wikipedia.org/wiki/Genetic_code)
+ http://en.wikipedia.org/wiki/Prokaryotic_DNA_replication
+ http://en.wikipedia.org/wiki/Origin_of_replication
+ [DNA polymerase](http://en.wikipedia.org/wiki/DNA_polymerase)
+ http://en.wikipedia.org/wiki/DNA_polymerase_III_holoenzyme
+ http://en.wikipedia.org/wiki/Divalent
+ https://en.wikipedia.org/wiki/DNA_codon_table

## Chromosomes
* The U.S. National Library of Medicine's Genetics Home Reference on [Chromosomes](http://ghr.nlm.nih.gov/chromosomes) 

## Sequence Analysis
* [Guide to SEQ analysis](https://www.ncbi.nlm.nih.gov/guide/sequence-analysis/)

----

# PAPERS
* [Sequence complexity and DNA curvature](http://research.haifa.ac.il/~genom/Alex%27s_page/article.pdf) by Gabrielian, A.E. & Bolshoy, A. in Comput Chem. 23, 263-274 (1999).
* [The demoiselle of X-inactivation: 50 years old and as trendy and mesmerising as ever](http://www.plosgenetics.org/article/info:doi/10.1371/journal.pgen.1002212), Morey C, Avner P. (2011). PLoS Genet 7(7):e1002212.
* [A comparative encyclopedia of DNA elements in the mouse genome](http://www.nature.com/nature/journal/v515/n7527/full/nature13992.html)
* [Principles of regulatory information conservation between mouse and human](http://www.nature.com/nature/journal/v515/n7527/full/nature13985.html)


