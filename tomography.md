


# Tomography
+ https://en.wikipedia.org/wiki/X-ray_computed_tomography
+ https://en.wikipedia.org/wiki/Tomography
+ The [tomographic reconstruction](http://en.wikipedia.org/wiki/Tomographic_reconstruction)
+ [Radon transform](http://en.wikipedia.org/wiki/Radon_transform) 
+ http://mathworld.wolfram.com/RadonTransform.html
+ Filtered back projection: http://www.owlnet.rice.edu/~elec539/Projects97/cult/node2.html
+ [FFT](http://en.wikipedia.org/wiki/Fast_Fourier_transform)
+ Matlab’s image registration toolbox: http://www.mathworks.com/discovery/image-registration.html 
__CT Data__
+ CT Images, https://www.nlm.nih.gov/research/visible/fresh_ct.html
   + https://mri.radiology.uiowa.edu/visible_human_datasets.html

### TomoPy
+ https://github.com/tomopy/tomopy, [Docs](https://tomopy.readthedocs.org/)
+ Homepage: https://www1.aps.anl.gov/Science/Scientific-Software/TomoPy
+ Paper: http://scripts.iucr.org/cgi-bin/paper?S1600577514013939
   + PDF copy of [TomoPy: a framework for the analysis of synchrotron tomographic data](http://journals.iucr.org/s/issues/2014/05/00/pp5049/pp5049.pdf) Doga Gursoy, Francesco De Carlo, Xianghui Xiao and Chris Jacobsen.
