# scientia 
My personal bibliography of useful resources, viz. books, research papers and [grey literature](https://en.wikipedia.org/wiki/Grey_literature) on [science](https://en.wikipedia.org/wiki/Outline_of_science), et al, with some interesting advice on [how to read a research paper](http://cseweb.ucsd.edu/~wgg/CSE210/howtoread.html).

----

# LICENSE 
+ COPYRIGHT © 2010-Now [SVAKSHA](http://svaksha.com/pages/Bio), All Rights Reserved. 
+ This work is licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License - (CC BY-NC-SA 4.0) as detailed in the [LICENSE.md](https://github.com/svaksha/scientia/blob/master/LICENSE.md) file and ALL copies and forks of this work must retain the Copyright, Licence and this permission notice.


### Mirrors
+ [GitLab](https://gitlab.com/svaksha/scientia) :: git clone git@gitlab.com:svaksha/scientia.git 
+ [Devlabs](https://gitlab.devlabs.linuxassist.net/svaksha/scientia) :: git clone ssh://git@gitlab.devlabs.linuxassist.net:608/svaksha/scientia.git


