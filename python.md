

# setup.py
+ https://github.com/cobbler/cobbler/blob/master/setup.py
+ https://docs.python.org/3.4/distutils/setupscript.html
+ http://stackoverflow.com/questions/5932804/set-file-permission-in-setup-py-file/25761434#25761434


# Conda
+ http://conda.pydata.org/docs/intro.html#conda-overview

# Tox
+ https://testrun.org/tox/latest/example/basic.html#depending-on-requirements-txt
+ https://testrun.org/tox/latest/config.html

# Julia-via-Anaconda
+ https://groups.google.com/forum/#!topic/julia-dev/-B90BrkzM6E
+ https://github.com/JuliaLang/IJulia

# Exceptions
+ http://stackoverflow.com/questions/21211006/how-to-catch-all-old-style-class-exceptions-in-python

# BAD programming habits
+ http://stackoverflow.com/questions/21553327/why-is-except-pass-a-bad-programming-practice

