* [Crystallography](#crystallography)
* [Genome](#genome)
* [General Datasets](#general-datasets)
* [Genetics-Medicine](#genetics-medicine)
* [Machine Learning](#machine-learning)
+ [Molecular Biology](#molecular-biology)
* [Neuroscience](#neuroscience)
* [Pharma](#Pharma)
* [Research Data](#research-data)
* [Scientific Databases](#scientific-databases)

**NB :: Not all the DB's are freely available.**

----

# Crystallography
* [Crystallographic databases](http://en.wikipedia.org/wiki/Category:Crystallographic_databases) list on WP.
* [Protein Data Bank](http://en.wikipedia.org/wiki/Protein_Data_Bank) (PDB) on WP.
* [Inorganic Crystal Structure Database](http://en.wikipedia.org/wiki/Inorganic_Crystal_Structure_Database) 

----

# Genome
* [BLAST](http://blast.ncbi.nlm.nih.gov/Blast.cgi) :: BLAST Assembled Genomes.
* [Chimpanzee Genome Project](http://en.wikipedia.org/wiki/Chimpanzee_Genome_Project)
* [DataLad](http://datalad.org) :: aims to provide access to scientific data available from various sources (e.g. lab or consortium web-sites such as Human connectome; data sharing portals such as OpenFMRI and CRCNS) through a single convenient interface and integrated with your software package managers (such as APT in Debian). Although initially targeting neuroimaging and neuroscience data in general, it will not be limited by the domain and a wide range of contributions are welcome. Get the [source code](https://github.com/datalad) on github.
* [dbGaP](https://www.ncbi.nlm.nih.gov/gap/) :: The database of Genotypes and Phenotypes (dbGaP) was developed to archive and distribute the results of studies that have investigated the interaction of genotype and phenotype.
* [Ensembl](http://en.wikipedia.org/wiki/Ensembl) DB.  
* [Ensembl Genomes](http://en.wikipedia.org/wiki/Ensembl_Genomes) DB.
* [Entrez](http://en.wikipedia.org/wiki/Entrez) DB.
   * [Entrez Programming Utilities](https://www.ncbi.nlm.nih.gov/books/NBK25500/) Help. 
* Genome project :: http://en.wikipedia.org/wiki/Genome_project
* [Genome Project Database](http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?DB=genomeprj)
* [Human Genome Project](http://en.wikipedia.org/wiki/Human_genome_project)
* [Human microbiome project](http://en.wikipedia.org/wiki/Human_microbiome_project)
* [Neanderthal genome project](http://en.wikipedia.org/wiki/Neanderthal_genome_project)
   - http://en.wikipedia.org/wiki/Neanderthal_extinction_hypotheses
   - http://en.wikipedia.org/wiki/Archaic_human_admixture_with_modern_humans   
* [NHLBI (National Heart, Lung, and Blood Institute)](http://www.nhlbi.nih.gov/research/resources/index.htm) resources, NIH (National Institutes of Health).
* [Personal Genome Project](http://en.wikipedia.org/wiki/Personal_Genome_Project)
* [Reactome](https://secure.wikimedia.org/wikipedia/en/wiki/Reactome) DB.
* [RefSeqGene](https://www.ncbi.nlm.nih.gov/refseq/rsg/) defines genomic sequences to be used as reference standards for well-characterized genes and is part of the Locus Reference Genomic (LRG) Project.
* The 3000 [Rice Genomes Project Data](http://dx.doi.org/10.5524/200001), GigaScience Database and [Journal](http://www.gigasciencejournal.com/content/3/1/8) and [blog article in BMC](See also: http://blogs.biomedcentral.com/gigablog/2014/05/29/publish-data-fight-world-hunger/).
* [Saccharomyces Genome Database](https://secure.wikimedia.org/wikipedia/en/wiki/Saccharomyces_Genome_Database)
* [Sequence Read Archive](http://www.ncbi.nlm.nih.gov/sra) (SRA) from NCBI and the [NCBI Genebank](https://www.ncbi.nlm.nih.gov/genbank/).

----

# General Datasets
* [Freebase](http://www.freebase.com) :: A community-curated database of well-known people, places, and things.
* [World Bank Open Data](http://data.worldbank.org) :: Free and open access to data about development in countries around the globe.

----

# Genetics-Medicine
* [Resources for Genetics and Medicine](https://www.ncbi.nlm.nih.gov/guide/genetics-medicine/]
* [HIV-1, Human Protein Interaction Database](http://www.ncbi.nlm.nih.gov/genome/viruses/retroviruses/hiv-1/interactions/) :: A database of known interactions of HIV-1 proteins with proteins from human hosts. It provides annotated bibliographies of published reports of protein interactions, with links to the corresponding PubMed records and sequence data.

----

# Machine Learning
+ [UCI Machine Learning Repository](http://archive.ics.uci.edu/ml/)

----

# Molecular Biology
+ [SASBDB](http://www.sasbdb.org/) ::Small Angle Scattering Biological Data Bank.
   + http://www.embl-hamburg.de/biosaxs/
   + http://www.embl-hamburg.de/biosaxs/software.html

----

# Neuroscience
* [OpenfMRI.org](https://openfmri.org) :: is a project dedicated to the free and open sharing of functional magnetic resonance imaging (fMRI) datasets, including raw data.
* [Neuroscience Databases](http://en.wikipedia.org/wiki/List_of_neuroscience_databases) list.
* [Neurovault](http://neurovault.org/) :: A place where researchers can publicly store and share unthresholded statistical maps produced by MRI and PET studies.

----

# Pharma
* [OSDD - open source drug discovery](http://www.osdd.org/).

----

# Research Data
* [Registry of Research Data Repositories](http://www.re3data.org/) :: provides researchers, funding organisations, libraries and publishers with over 1,000 listed research data repositories from all over the world making it the largest and most comprehensive online catalog of research data repositories on the web.

----

# Scientific Databases
* [Scientific Databases](http://en.wikipedia.org/wiki/Category:Scientific_databases) list on WP.
* [Chemical DB](http://en.wikipedia.org/wiki/Category:Chemical_databases) list on WP.
