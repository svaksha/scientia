* [BioMed](#biomed)
* [Epidemiology](#epidemiology)
* [MedChem](#medchem)
+ [Psychology-Cognitive](#psychology-cognitive)

----

# BioMed
* [Understanding the barriers to identifying carers of people with advanced illness in primary care: triangulating three data sources](http://europepmc.org/articles/PMC3992158). 
* 10 Algos : http://healthsciences.utah.edu/innovation/tenalgorithms/index.php
* The top 100 papers : http://www.nature.com/news/the-top-100-papers-1.16224?WT.mc_id=TWT_NatureNews#/b8

----
 
# Epidemiology
* [The changing risk of Plasmodium falciparum malaria infection in Africa: 2000–10: a spatial and temporal analysis of transmission intensity](http://europepmc.org/articles/PMC4030588)

----

# MedChem
* The [Discovery of tetrahydropyrazolopyrimidine carboxamide derivatives as potent and orally active antitubercular agents](http://europepmc.org/articles/PMC4027361/), ACS Med Chem Lett. May 9, 2013; Published online Apr 1, 2013. doi:  10.1021/ml400071a.

----

# Psychology-Cognitive
+ https://en.wikipedia.org/wiki/List_of_cognitive_biases

