* [Journals](#journals)
* [NEWS](#news)

----

# Journals
* [Society for Neuroscience](http://eneuro.sfn.org/types.html) OpenAccess Journal.
* [Europe PubMed Central](http://europepmc.org/).

----

# NEWS
* [France Prefers to Pay (twice) for Papers by Its Researchers](http://blog.okfn.org/2014/11/11/france-prefers-to-pay-twice-for-papers-by-its-researchers/).
* [Brace Yourself for Large-Scale Whole Genome Sequencing](http://massgenomics.org/2014/11/brace-yourself-for-large-scale-whole-genome-sequencing.html)
* [Trialling a pragmatic approach to clinical data disclosure](http://blogs.nature.com/scientificdata/2014/11/27/trialling-a-pragmatic-approach-to-clinical-data-disclosure/)

