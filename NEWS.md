+ [Business Analytics](#business-analytics)
+ [FOSS License](#foss-license)
+ [Organizations](#organizations)
+ [LINKS](#links)
   + [How2HireDataScientists](#how2hiredatascientists)
+ [Software Management](#software-management)

----

# Business Analytics
+ [Predictive analytics: the next bigthing in BI?](http://www.rosebt.com/uploads/8/1/8/1/8181762/predictive_analytics_e_guide.pdf)

----

# FOSS License
+ Github repos that have no license are : http://choosealicense.com/no-license/
+ http://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses
+ [Copyright](https://en.wikipedia.org/wiki/Clean_room_design)

----

# Organizations
+ A list of [UK-based professional/ scientific organisations](https://docs.google.com/spreadsheets/d/1haSsyhY6bqjXJHIiCNaOCIqgCqmQqUVQEv55vewu93I/edit#gid=0)

----

# LINKS

+ How 2 Hire Data Scientists, http://firstround.com/review/how-to-consistently-hire-remarkable-data-scientists/
+ http://yourstory.com/2015/04/doormint/

----

# [Software Management](http://en.wikipedia.org/wiki/Category:Software_project_management)
+ Use Case: http://en.wikipedia.org/wiki/Use_case
+ Algo usecase, http://www.w3.org/2005/Incubator/mmsem/wiki/Algorithm_representation_Use_case.html
+ http://en.wikipedia.org/wiki/Taxonomy
+ Info Sc. ontology, http://en.wikipedia.org/wiki/Ontology_%28information_science%29
+ Formal concept analysis: http://en.wikipedia.org/wiki/Formal_concept_analysis
