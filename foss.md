

# Dotclear-DocuWiki
+ Dotclear wiki syntax : http://dotclear.org/documentation/2.0/usage/syntaxes
+ Formatting syntax : http://fr.dotclear.org/documentation/wiki/syntax
+ http://fr.dotclear.org/documentation/wiki/dokuwiki

----

# LaTeX
+ Prof. Edward R. Scheinerman's resources to "[Learn LaTeX](http://www.ams.jhu.edu/~ers/learn-latex/)".
+ http://tex.stackexchange.com/questions/8374/the-key-to-understanding-the-latex-syntax
+ latex for MATH: http://en.wikibooks.org/wiki/LaTeX/Mathematics
+ LaTeX tutorial : http://www.pages.drexel.edu/~pyo22/students/latexRelated/latexTutorial.html
+ http://latex-project.org/intro.html

----

# ARBTT 
__desktop timetracker__
+ https://www.joachim-breitner.de/blog/336-The_Automatic_Rule-Based_Time_Tracker
+ GITHUB mirror : 
+ http://arbtt.nomeata.de/#how
+ Install : http://arbtt.nomeata.de/#install
+ Sample Categorize :: http://darcs.nomeata.de/arbtt/categorize.cfg
+ http://stackoverflow.com/questions/20973590/arbtt-nested-if-then-else-in-categorize-cfg/20973950#20973950
+ https://wiki.haskell.org/Unicode-symbols
+ http://darcs.nomeata.de/arbtt/doc/users_guide/arbtt-stats.html#idp213048

----

# MOZILLA
## SWC-MSF
+ https://swcarpentry.etherpad.mozilla.org/instructors-Europe?

## Accounts
+ [Dev](https://developer.mozilla.org)
+ [Wiki](https://wiki.mozilla.org/)
+ [bugzilla](https://bugzilla.mozilla.org/)
+ [MozOrg on Github](https://github.com/mozilla/)
+ https://en.wikipedia.org/wiki/Category:Mozilla

## Firefox OS
+ https://en.wikipedia.org/wiki/Category:Firefox_OS

----

# Linux : List Open Files for Process
+ http://www.cyberciti.biz/faq/howto-linux-get-list-of-open-files/
