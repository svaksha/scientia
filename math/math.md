
+ [Arithmetic](#Arithmetic)
+ [Calculus and Analysis](#calculus-and-analysis)
+ [Linear Algebra](#linear-algebra)

---- 

# Arithmetic
+ [A "two-product" style algorithm paper](http://www.ti3.tu-harburg.de/paper/rump/OgRuOi05.pdf).

----

# Calculus and Analysis
+ https://en.wikipedia.org/wiki/Lambda_calculus

### Integral Transforms
+ https://en.wikipedia.org/wiki/Integral_geometry
+ http://mathworld.wolfram.com/IntegralTransform.html

---- 

# Geometry
+ [Chirality in MATH](http://en.wikipedia.org/wiki/Chirality_%28mathematics%29) and [Chirality in Organic Chemistry](http://en.wikipedia.org/wiki/Chirality_%28chemistry%29).
+ http://mathworld.wolfram.com/Slope.html
+ https://en.wikipedia.org/wiki/Integral_geometry

### Papers
+ http://arxiv.org/pdf/1404.1499v2.pdf

----

### sparse distributed matrix
+ https://en.wikipedia.org/wiki/Array_data_type
+ https://ch.mathworks.com/help/distcomp/sparse.html
+ https://ch.mathworks.com/help/distcomp/distributed.spalloc.html
