
# Linear Algebra
+ https://www.khanacademy.org/math/linear-algebra
   + Video : https://www.khanacademy.org/math/linear-algebra/matrix_transformations/linear_transformations/v/vector-transformations
+ https://en.wikipedia.org/wiki/Linear_algebra
+ https://en.wikipedia.org/wiki/Cholesky_decomposition
+ https://en.wikipedia.org/wiki/LU_decomposition
+ https://en.wikipedia.org/wiki/Matrix_%28mathematics%29
+ [Matrix and Linear Algebra](http://www.economics.soton.ac.uk/staff/aldrich/matrices.htm)


