# PD Dr. Jürgen Schmitz


### https://www.uni-muenster.de/forschungaz/habilitation/504
With the current increasing availability of fully sequenced genomes, more and more investigations encompass the analyses of full genomes, but still do not fully resolve an organism's evolutionary history. To avoid the problems of homoplasy (a molecular effect comparable to phenotypic convergence), Prof.Dr. Jürgen Schmitz and his team of scientists have turned to a virtually homoplasy-free marker system, the ancestral genomic insertions of transposable elements (TEs) to resolve evolutionary trees. This methodology compensates for the shortcomings of other systems, but requires high throughput computational and experimental screenings and very critical analyses of orthology. Even when this marker system does not deliver consistent evolutionary patterns, the inconsistencies themselves provide a unique tool to detect the effects of incomplete lineage sorting or hybridization and thereby to more easily distinguish true polytomy from dichotomous speciation events. Selected, optimized search strategies to find phylogenetically informative retrotransposon markers enable us to reap valuable historical information from this unique marker system. 

- https://www.uni-muenster.de/forschungaz/person/13022?lang=en

# other links
- http://www.uni-muenster.de/Evolution/mgse/ett/index.html
- http://research.haifa.ac.il/~genom/Alex%27s_page/index2.html
- http://en.wikipedia.org/wiki/Ram_Samudrala

# Community Forums
- http://seqanswers.com/



