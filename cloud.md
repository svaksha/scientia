
+ http://en.wikipedia.org/wiki/Category:Cloud_infrastructure
+ http://en.wikipedia.org/wiki/Category:Cloud_computing_providers
+ http://en.wikipedia.org/wiki/Comparison_of_open-source_configuration_management_software
+ http://docs.python-guide.org/en/latest/scenarios/admin/

----

# AMQP
+ http://www.amqp.org/
+ Disque, https://github.com/antirez/disque

## Celery
+ Celery, https://github.com/celery/celery
+ http://docs.celeryproject.org/en/latest/getting-started/first-steps-with-celery.html#rabbitmq


## ZeroMQ
+ ZeroMQ, https://github.com/zeromq/pyzmq
+ http://zeromq.org/intro:get-the-software
+ zguide, http://zguide.zeromq.org/page:all
+ guide, http://zguide.zeromq.org/py:all
   + Julia, https://github.com/imatix/zguide/tree/master/examples/Julia
   + Python, https://github.com/imatix/zguide/tree/master/examples/Python
   
   
## RabbitMQ
+ RabbitMQ, https://www.rabbitmq.com           # Erlang AMQP lib
+ Pika, https://github.com/pika/pika       # a Pure Python RabbitMQ/AMQP 0-9-1 client library.

https://www.rabbitmq.com/tutorials/tutorial-two-python.html
http://www.rabbitmq.com/configure.html#config-items
http://www.rabbitmq.com/memory.html#diskfreesup
http://pika.readthedocs.org/en/0.9.14/intro.html


----

# System Disk Monitoring tools
+ https://en.wikipedia.org/wiki/Comparison_of_network_monitoring_systems

+ https://pypi.python.org/pypi/psutil
+ https://github.com/nicolargo/glances
+ https://github.com/google/grr
+ https://github.com/python-diamond/Diamond
+ https://github.com/Jahaja/psdash
+ https://github.com/prometheus
+ http://www.zenoss.com
+ https://en.wikipedia.org/wiki/Ganglia_%28software%29
+ https://en.wikipedia.org/wiki/Icinga
+ https://en.wikipedia.org/wiki/Nagios


----

# Chef (.rb)
+ https://github.com/chef/chef
+ http://stackful-dev.com/cuisine-the-lightweight-chefpuppet-alternative

----

# Puppet (.rb)
+ http://en.wikipedia.org/wiki/Puppet_%28software%29

----

# Vagrant (.rb)
+ https://en.wikipedia.org/wiki/Vagrant_%28software%29

# Cobbler (.py)
+ https://en.wikipedia.org/wiki/Cobbler_%28software%29
+ https://github.com/cobbler/cobbler

----

# [OpenStack](http://openstack.org)
+ Nova, https://github.com/openstack/nova
+ http://www.openstack.org/software/
+ [OpenStack drivers](http://www.openstack.org/marketplace/drivers/)
+ http://www.ibm.com/developerworks/cloud/library/cl-openstack-pythonapis/

----

# AWS
+ http://aws.amazon.com 
+ http://en.wikipedia.org/wiki/Amazon_Web_Services
+ http://en.wikipedia.org/wiki/Amazon_Elastic_Compute_Cloud

## HELP
+ REF : http://docs.aws.amazon.com/general/latest/gr/Welcome.html
+ http://docs.aws.amazon.com/general/latest/gr/rande.html
+ https://aws.amazon.com/documentation/
+ CLI tools : http://docs.aws.amazon.com/general/latest/gr/GetTheTools.html

### SO
+ http://stackoverflow.com/questions/10526345/ec2-command-line-tools   

----

# Rackspace
+ https://github.com/rackerlabs

----

# DELL
+ https://support.enstratius.com/home
 
## mixcoatl
#### Wiki
+ Wiki :: https://github.com/enStratus/mixcoatl/wiki/Mixcoatl-0.10.32

#### Bugs 
+ https://github.com/enStratus/mixcoatl/issues/207
   + https://github.com/enStratus/mixcoatl/search?utf8=%E2%9C%93&q=customerId
   + https://github.com/enStratus/mixcoatl/blob/fa6dad007a3e2c07763660e21cca19b81fd73723/tests/data/region.py
+ https://github.com/irvingpop/enstratius-api-tools
+ https://github.com/zomGreg/riak-cli-tool

----

# DOCKER
+ https://www.digitalocean.com/community/tutorials/docker-explained-how-to-containerize-python-web-applications
+ https://books.google.co.in/books?id=oAkZBQAAQBAJ&pg=PA116&lpg=PA116&dq=python+code+to+spin+up+docker+containers&source=bl&ots=dSp_ddqppk&sig=BfOcQ7hY1oBndYT3DtVbgaT8dis&hl=en&sa=X&ei=Q9EcVeroFI-0uATYvoKYBg&ved=0CEcQ6AEwBw#v=onepage&q=python%20code%20to%20spin%20up%20docker%20containers&f=true
+ http://phusion.github.io/baseimage-docker/
+ https://pypi.python.org/pypi/docker-compose/1.2.0rc3

----

# HEROKU
+ https://addons.heroku.com/
+ https://devcenter.heroku.com/articles/getting-started-with-python
+ https://devcenter.heroku.com/articles/procfile
+ https://devcenter.heroku.com/categories/heroku-architecture
+ https://id.heroku.com/login

----

##### NEWS
+ http://www.networkworld.com/article/2160016/cloud-computing/who-makes-up-openstack-.html
+ https://training.linuxfoundation.org/sysadmin-evolution
+ http://www.networkworld.com/article/2160163/cloud-computing/12-free-cloud-storage-options.html
+ http://probably.co.uk/puppet-vs-chef-vs-ansible.html

